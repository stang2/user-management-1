/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THANAWAT_TH
 */
public class UserService {
    private static ArrayList<User> UserList = new ArrayList<User>();
    private static User currentUser = null;
    private static User SuperAdmin = new User("super","super");
    static{
       load();
    }
    //Create (C)
    public static boolean addUser(User user){
        UserList.add(user);
        save(); 
        return true;
    }
    //Delete (D)   
    public static boolean delUser(User user){
        UserList.remove(user);
        save();
        return true;
    }
    public static boolean delUser(int index){
        UserList.remove(index);
        save();
        return true;
    }
    //Read (R)
    public static ArrayList<User> getUser(){
        return UserList;
    }
    public static User getUser(int index){
        return UserList.get(index);
    }
    //Update (U)
    public static boolean updateUser(int index, User user){
        UserList.set(index, user);
        save();
        return true;
    }
    // File (F)
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("User.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(UserList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("User.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            UserList = (ArrayList<User>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static User Login(String UserName,String Password){
        if(UserName.equals("super") && Password.equals("super")){
            return SuperAdmin;
        }
        for(int i = 0;i < UserList.size();i++){
            User User = UserList.get(i);
            if(User.getUserName().equals(UserName) && User.getPassword().equals(Password)){
                currentUser = User;
                return User;
            }
        }
        return null;
    }
    public static boolean isLogin(){
        return currentUser != null;
    }
    public static User getCurrentUser(){
        return currentUser;
    }
    public static void logout(){
        currentUser = null;
    }
}
