package com.thanawat.usermanagement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANAWAT_TH
 * User Model
 */
public class User implements Serializable{
    private String UserName;
    private String Password;

    public User(String UserName, String password) {
        this.UserName = UserName;
        this.Password = password;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    @Override
    public String toString() {
        return "UserName=" + UserName + ", Password=" + Password;
    }
    
}
